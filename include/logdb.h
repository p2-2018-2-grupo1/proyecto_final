#ifndef LOGDB_H
#define LOGDB_H

/*
 * Esta es la interfaz de la que hara uso el cliente para conectarse y realizar las operaciones
 * en la base de datos remota logdb. ESTAS FUNCIONES NO SON PARTE DEL SERVIDOR logdb. Estas funciones
 * ENVIAN las solicitudes necesarias al servidor para hacer la operacion, el servidor las procesa,
 * y el servidor responde. Luego, estas funciones toman la respuesta del servidor y retornan los valores
 * necesarios al programa cliente. Basicamente, esta funciones toman informacion del cliente, la envian,
 * esperan la respuesta, la procesan y retornan.
 */ 

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>


//Estructuras 
typedef struct conexionlogbTDA{
    char *ip;			//IP en formato 123.123.123.123.
    unsigned short puerto;              
    int id_sesion;		//Inicialmente en -1. Cuando se llama a conectar_db(), esta funcion debe setear este valor (que recibio desde el servidor).
				//Al abrir la conexion, se llenar� este campo con un numero aleatorio (Este numero lo genera el proceso logdb, y lo devuelve a la lib).             
    char *nombredb;		//nombre de la db en uso. Inicialmente vacio hasta que se llame abrir_db(). Se llena SOLO si el proceso logdb pudo 
				//Abrir exitosamente los archivos de la DB (log e indice.
    int sockfd;         //Descriptor de socket de la conexion.
   
} conexionlogdb;

//Interfaz

conexionlogdb *conectar_db(char *ip, int puerto);			//Devuelve un objeto conexion, NULL si no se pudo abrir conexion.

int crear_db(conexionlogdb *conexion, char *nombre_db);			//Recibe un objeto conexion, y crear la db con el nombre dado. Devuelve 0 en exito, -1 en error (por ejemplo,
									//ya existe un base de datos con ese nombre, error de E/S, etc).

int abrir_db(conexionlogdb *conexion, char *nombre_db);			//Recibe un objeto conexion. Con esto se informa al proceso logdb que abra la base de datos nombre_db.
									//Retorna 0 si se pudo abrir, -1 en error (por ejemplo, si dicha db no existe).

int put_val(conexionlogdb *conexion, char *clave, char *valor);		//Inserta el elemento clave:valor en la db. Devuelve 0 en exito, -1 en  error.

char *get_val(conexionlogdb *conexion, char *clave);			//devuelve el valor asociado a clave (si existe). Devuelve NULL si la clave no existe.

int eliminar(conexionlogdb *conexion, char *clave);			//Elimina la clave. Devuelve 0 en exito, -1 en error.

void cerrar_db(conexionlogdb *conexion);				//Cierra la conexion y libera la estructura conexion.


#endif
