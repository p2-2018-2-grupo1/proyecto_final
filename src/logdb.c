//Programa libreria 
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<errno.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>


const int CREAR = 1001;
const int ABRIR = 1010;
const int PUT = 1100;
const int GET = 1101;
const int ELIMINAR = 1110;
const int CERRAR = 1111;

typedef struct conexionlogbTDA{
    char *ip;			//IP en formato 123.123.123.123.
    unsigned short puerto;              
    int id_sesion;		//Inicialmente en -1. Cuando se llama a conectar_db(), esta funcion debe setear este valor (que recibio desde el servidor).
				//Al abrir la conexion, se llenar� este campo con un numero aleatorio (Este numero lo genera el proceso logdb, y lo devuelve a la lib).             
    char *nombredb;		//nombre de la db en uso. Inicialmente vacio hasta que se llame abrir_db(). Se llena SOLO si el proceso logdb pudo 
				//Abrir exitosamente los archivos de la DB (log e indice.
    int sockfd;         //Descriptor de socket de la conexion.
} conexionlogdb;

int open_socket(int domain, int type, int protocol, const struct sockaddr* direccion){
    int fd;
    if (( fd = socket( domain, type, protocol)) < 0) 
        return -1;
    if (connect( fd, direccion, (socklen_t)sizeof(*direccion)) == 0) /* * Conexión aceptada. */ 
        return(fd); 
    close(fd); 				//Si falla conexion cerramos
	return(-1); 
}

conexionlogdb *conectar_db(char* ip, int puerto){
    //creación del socket
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(puerto);
    server_address.sin_addr.s_addr = inet_addr(ip);
    
    int sockfd = open_socket(server_address.sin_family, SOCK_STREAM, 0,(struct sockaddr*)&server_address);
    if(sockfd < 0)
        return NULL;

    conexionlogdb* conexion = malloc(sizeof(conexionlogdb));
    memset(conexion, 0, sizeof(*conexion));
    conexion->ip = ip;
    conexion->puerto = (unsigned short)puerto;
    conexion->id_sesion = -1;
    conexion->nombredb = "";
    conexion->sockfd = sockfd;
    return conexion;
}

int crear_db(conexionlogdb *conexion, char *nombre_db){
    if(conexion == NULL)
        return -1;

    int var = htonl(CREAR);
    ssize_t size = send(conexion->sockfd, &var, sizeof(int), 0); //envio solicitud de crear la base de datos
    if(size < 0)
        return -1;
    sleep(1);//para evitar que el proceso cliente le gane al proceso servidor
    size = send(conexion->sockfd, nombre_db, strlen(nombre_db)*sizeof(char), 0); //envio de nombre de la base de datos
    if(size < 0)
        return -1; // fallo al enviar nombre

    char* buf[128];
	memset(buf, '\0', 128);
    ssize_t bytes = recv(conexion->sockfd, buf, 128, 0);
    if(bytes < 0)
        return -1;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error

    /*if (resp == 0)
        conexion->nombredb = nombre_db;*/
    return resp;
}

int abrir_db(conexionlogdb *conexion, char *nombre_db){
    if(conexion == NULL){
        return -1;
    }
    
    int var = htonl(ABRIR);
    ssize_t size = send(conexion->sockfd, &var, sizeof(int), 0); //envio solicitud de abrir base de datos
    if(size < 0)
        return -1;
    
    sleep(1);//para evitar que el proceso cliente le gane al proceso servido
    size = send(conexion->sockfd, nombre_db, strlen(nombre_db)*sizeof(char), 0); //envio de nombre de la base de datos
    if(size < 0)
        return -1; // fallo al enviar nombre
    
    char* buf[128];
	memset(buf, '\0', 128);
    ssize_t bytes = recv(conexion->sockfd, buf, 128, 0);
    if(bytes < 0)
        return -1;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error

    if (resp == 0)
        conexion->nombredb = nombre_db;
    return resp;
}   

int put_val(conexionlogdb *conexion, char *clave, char *valor){
    if(conexion == NULL)
        return -1;
    if(strcmp(conexion->nombredb, "") == 0) //en caso que no se haya hecho open anteriormente
        return -1;
    int sol = htonl(PUT);
    ssize_t n = send(conexion->sockfd, &sol, sizeof(int), 0);
    if(n < 0)
        return -1;
    sleep(1);
    ssize_t n1 = send(conexion->sockfd, conexion->nombredb, strlen(conexion->nombredb)*sizeof(char), 0);
    sleep(1);
    ssize_t c = send(conexion->sockfd, clave, strlen(clave)*sizeof(char), 0);
    sleep(1);
    ssize_t v = send(conexion->sockfd, valor, strlen(valor)*sizeof(char), 0);

    if(n1 < 0 || c < 0 || v < 0)
        return -1;

    char* buf[128];
	memset(buf, '\0', 128);
    ssize_t bytes = recv(conexion->sockfd, buf, 128, 0);
    if(bytes < 0)
        return -1;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error

    return resp;
}

char *get_val(conexionlogdb *conexion, char *clave){
    if(conexion == NULL || strcmp(conexion->nombredb, "") == 0){
        return (void *)0;
    }
    
    int var = htonl(GET);
    ssize_t size = send(conexion->sockfd, &var, sizeof(int), 0); //envio solicitud de obetener de la base de datos
    if(size < 0)
        return NULL;
    sleep(1);//para evitar que el proceso cliente le gane al proceso servidor
    size = send(conexion->sockfd, clave, strlen(clave)*sizeof(char), 0); //envio de la clave a la base de datos
    sleep(1);
    ssize_t n1 = send(conexion->sockfd, conexion->nombredb, strlen(conexion->nombredb)*sizeof(char), 0); // enviar el nombre de la DB
    if(size < 0 || n1 < 0)
        return NULL; //fallo al enviar la clave

    char* buf = malloc(sizeof(char)*1000);
	memset(buf, '\0', 1000);
    size = recv(conexion->sockfd, buf, 1000, 0);
    if(size < 0)
        return NULL;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error
    if (resp == -1){
        return NULL;
    }
    
    memset(buf, '\0', 1000);
    size = recv(conexion->sockfd, buf, 1000, 0);
    if(size < 0)
        return NULL;
        
    return buf;
}

int eliminar(conexionlogdb *conexion, char *clave){
    if(conexion == NULL)
        return -1;
    
    if(strcmp(conexion->nombredb, "") == 0) //en caso que no se haya hecho open anteriormente
        return -1;
    int sol = htonl(ELIMINAR);
    ssize_t n = send(conexion->sockfd, &sol, sizeof(int), 0);
    if(n < 0)
        return -1;
    sleep(1);
    ssize_t n1 = send(conexion->sockfd, conexion->nombredb, strlen(conexion->nombredb)*sizeof(char), 0);
    sleep(1);
    ssize_t c = send(conexion->sockfd, clave, strlen(clave)*sizeof(char), 0);

    if(n1 < 0 || c < 0)
        return -1;

    char* buf[128];
	memset(buf, '\0', 128);
    ssize_t bytes = recv(conexion->sockfd, buf, 128, 0);
    if(bytes < 0)
        return -1;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error
    return resp;
}

void cerrar_db(conexionlogdb *conexion){
    if(conexion == NULL || strcmp(conexion->nombredb, "") == 0){
        return ;
    }
    int var = htonl(CERRAR);
    ssize_t size = send(conexion->sockfd, &var, sizeof(int), 0); //envio solicitud de cerrrar la base de datos
    if(size < 0)
        return ;
    sleep(1);//para evitar que el proceso cliente le gane al proceso servidor
    size = send(conexion->sockfd, conexion->nombredb, strlen(conexion->nombredb)*sizeof(char), 0);// enviar el nombre de la DB
    if(size < 0 )
        return ; //fallo al enviar la clave

    char* buf[128];
	memset(buf, '\0', 128);
    ssize_t bytes = recv(conexion->sockfd, buf, 128, 0);
    if(bytes < 0)
        return ;
    int* ptr = (int*)buf;
    int resp = ntohl(*ptr); //sevidor devuelve 0 en exito y -1 en error
    if(resp == 0){
        free(conexion);
        conexion = NULL;
    }
}