//Cliente de Prueba
#include <stdio.h>
#include <logdb.h>

int main(){

    conexionlogdb* conexion = conectar_db("127.0.0.1", 4040);
    if(conexion == NULL){
        perror("Error");
        return -1;
    }

    /*int res = crear_db(conexion, "datos");
    if(res < 0){
        printf("Error al crear base de datos\n");
    }*/

    int resabr = abrir_db(conexion, "datos");
    if(resabr < 0){
        printf("Error al abrir base de datos\n");
    }

    char * valor = get_val(conexion, "nombre");
    if(valor != NULL)
        printf("El valor de la clave nombre es %s\n",valor);
    else
        printf("No se encuetra la clave nombre\n");
    valor = get_val(conexion, "edad");
    if(valor != NULL)
        printf("El valor de la clave edad es %s\n",valor);
    else
        printf("No se encuetra la clave edad\n");
    
    int result = eliminar(conexion, "edad");
    if(result < 0)
        printf("Error al eliminar\n");
    else
        printf("Eliminado con exito\n");

    valor = get_val(conexion, "edad");
    if(valor != NULL)
        printf("El valor de la clave edad es %s\n",valor);
    else
        printf("No se encuetra la clave edad\n");

    int result2 = eliminar(conexion, "edad");
    if(result2 < 0)
        printf("Error al eliminar\n");
    else
        printf("Eliminado con exito\n");

    int r = put_val(conexion, "edad", "21");
    if(r < 0)
        printf("Error al escribir en la base de datos\n");
    else
        printf("Se escribio correctamente Edad:21\n");

    printf("ip: %s\n", conexion->ip);
    printf("puerto: %d\n", conexion->puerto);
    printf("id_sesion: %d\n", conexion->id_sesion);
    printf("nombredb: %s\n", conexion->nombredb);
    printf("sockfd: %d\n", conexion->sockfd);

    cerrar_db(conexion);
    return 0;
}