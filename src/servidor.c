//programa servidor
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<errno.h>
#include<unistd.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<string.h>
#include<uthash.h>
#include<stdio.h>

#define TRUE 1
#define FALSE 0

int CREAR = 1001;
int ABRIR = 1010;
const int PUT = 1100;
const int GET = 1101;
const int ELIMINAR = 1110;
const int CERRAR = 1111;

typedef struct indices{
	char clave[250];
	int valor;
	UT_hash_handle hh;		//necesario para poder usar la estrutura en la hashtable.
}indices_struct;

indices_struct *tabla = NULL;
char* concat(char*ubicacion, char* nombre); //concatena ubicacion con nombre
int createDataBase(char* ubicacion, char* nombre); //crea una base de datos
int openDataBase(char* ubicacion, char* nombre); //abre una base de datos existente
int putValue(char*ubicacion, char* nombre, char* key, char*value);
char* getValue(char*ubicacion, char* nombre, char* key);
int delete(char* ubicacion, char* nombre, char* key);

int init(const struct sockaddr* direccion, socklen_t alen){
    int fd, err = 0;
    fd = socket(direccion->sa_family, SOCK_STREAM, 0);
    if(fd < 0) 
        return -1;
    if(bind(fd, direccion, alen) < 0)
        goto errout;
    if(listen(fd, 1000) < 0)
        goto errout;
    return fd;

    errout:
        err = errno;
        close(fd);
        errno = err;
        return (-1);
}

int main(int argc, char** argv){
    if(argc != 4){
        printf("Numero incorrecto de argumentos\n");
        printf("Sintaxis: logfb <directory> <IP address> <port>\n");
        return -1;
    }

    char* directory = argv[1];
    char* ip = argv[2];
    int port = atoi(argv[3]);
    int abierta = 1;
    
    struct sockaddr_in server_address;
    memset(&server_address, 0, sizeof(server_address));
    server_address.sin_family = AF_INET;
    server_address.sin_port = htons(port);
    server_address.sin_addr.s_addr = inet_addr(ip);
    
    int sockfd = init((struct sockaddr*)&server_address, sizeof(server_address));
    if(sockfd < 0){
        perror("Error al inicializar servidor");
        return -1;
    }

    int sfd;
    while(TRUE){
        if(abierta){
            printf("ESTADO: Esperando cliente\n");
            sfd = accept(sockfd, NULL, NULL);
            printf("Servidor conectado a (%s)\n", inet_ntoa(server_address.sin_addr));
            abierta = 0;
        }
        
        char buf[128]; //buffer para leer peticiones de un cliente
	    memset(buf, '\0', 128*sizeof(char));
        ssize_t bytes = recv(sfd, buf, 128, 0);
        if(bytes < 0){
            perror("Error al recibir solicitud");
			int num = htonl(-1);		//Envio de un entero
			send(sfd, &num, sizeof(int), 0);
        }else{
            int* ptr = (int*)buf;           //Transformar el numero recido int ptr
            int solicitud = ntohl(*ptr);    //de red a host
            printf("Exito solicitud %d\n",solicitud);
            switch(solicitud){
                case 1001:{
                    printf("Crear base de datos: %d\n", solicitud);
                    //recibir el nombre de la base de datos
                    char nombre[800];
                    memset(nombre, '\0', 800);
                    ssize_t n = recv(sfd, nombre, 500, 0);
                    if(n < 0){
                        perror("Error al recibir nombre de la base de datos a crear");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        //Crear la base de datos
                        printf("nombre: %s\n", nombre);
                        int r = createDataBase(directory, nombre); //devuelve 0 o -1
                        if(r < 0){
                            perror("Error al crear base de datos");
                        }
                        //Respuesta del servidor
                        int num = htonl(r);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }
                    break;
                }
                case 1010:{
                    printf("Abrir base de datos: %d\n", solicitud);
                    char nombre[800];
                    memset(nombre, '\0', 800);

                    ssize_t n = recv(sfd, nombre, 500, 0);  //recibir el nombre de la base de datos
                    if(n < 0){
                        perror("Error al recibir nombre de la base de datos");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        printf("Base de Datos: %s\n", nombre);
                        int r = openDataBase(directory, nombre); //devuelve 0 o -1
                        if(r<0)
                            perror("Error al cargar la informacion de indices ");

                        int num = htonl(r);		//Envio de un entero en respuesta al cliente
                        send(sfd, &num, sizeof(int), 0);
                    }
                    break;
                }
                case 1100:
                    printf("Agregar elemento a la base de datos: %d\n", solicitud);
                    char nomb[800];
                    char clave[800];
                    char valor[800];
                    memset(nomb, '\0', 800);
                    memset(clave, '\0', 800);
                    memset(valor, '\0', 800);
                    ssize_t n1 = recv(sfd, nomb, 800, 0);
                    ssize_t c = recv(sfd, clave, 800, 0);
                    ssize_t v = recv(sfd, valor, 800, 0);

                    if(n1 < 0 || c < 0 || v < 0){
                        perror("Error al recibir datos");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        printf("Nombre: %s\tClave: %s\tValor: %s\n", nomb, clave, valor);                  
                        int r = putValue(directory, nomb, clave, valor);

                        if(r<0)
                            perror("Error al agregar nuevo valor");

                        int num = htonl(r);		//Envio de un entero en respuesta al cliente
                        send(sfd, &num, sizeof(int), 0);
                    }
                    break;
                case 1101:{
                    printf("Obtener de la base de datos: %d\n", solicitud);
                    char clave[800];
                    char nomb[800];
                    memset(nomb, '\0', 800);
                    memset(clave, '\0', 800);

                    ssize_t n = recv(sfd, clave, 500, 0);  //recibir la clave de la base de datos
                    ssize_t n1 = recv(sfd, nomb, 800, 0);  //recibir el nombre de la base de datos
                    if(n < 0 || n1 < 0){
                        perror("Error al recibir los datos");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        char* r = getValue(directory, nomb, clave); //devuelve 0 o -1
                        if(r == NULL){
                            perror("Error al obtener el valor de la clave");
                            int num = htonl(-1);		//Envio de un entero
			                send(sfd, &num, sizeof(int), 0);
                        }else{
                            int num = htonl(0);		//Envio de un entero
			                send(sfd, &num, sizeof(int), 0);
                            sleep(1);
                            send(sfd, r, sizeof(r), 0);
                        }
                    }                    
                    break;
                }
                case 1110:{
                    printf("Eliminar elemento de la base de datos: %d\n", solicitud);
                    char nombre[800];
                    char clave[800];
                    memset(nombre, '\0', 800);
                    memset(clave, '\0', 800);
                    ssize_t n1 = recv(sfd, nombre, 800, 0);
                    ssize_t c = recv(sfd, clave, 800, 0);

                    if(n1 < 0 || c < 0){
                        perror("Error al recibir datos");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        printf("Eliminando...\n");
                        int r = delete(directory, nombre, clave);
                        if(r < 0)
                            printf("Error al eliminar elemento\n");
                        else   
                            printf("Eliminado con exito\n");

                        int num = htonl(r);		//Envio de un entero en respuesta al cliente
                        send(sfd, &num, sizeof(int), 0);
                    }
                    break;
                }
                case 1111:{
                    printf("Cerrar base de datos: %d\n", solicitud);
                    char nomb[800];
                    memset(nomb, '\0', 800);

                    ssize_t n1 = recv(sfd, nomb, 800, 0);  //recibir el nombre de la base de datos
                    if(n1 < 0){
                        perror("Error al recibir el nombre de la DB");
			            int num = htonl(-1);		//Envio de un entero
			            send(sfd, &num, sizeof(int), 0);
                    }else{
                        abierta = 1;
                        tabla = NULL;
                        int num = htonl(0);		//Envio de un entero en respuesta al cliente
                        send(sfd, &num, sizeof(int), 0);
                        printf("Base de datos %s cerrada!\n",nomb);
                    }
                    break;
                }
                default:
                    printf("Opcion no valida %d\n", solicitud);
                    abierta = 1;
                    tabla = NULL;
                    printf("No se abrió la base de datos, se cerrará la conexion!\n");
                    break;
            }           
        }
        printf("\n");
    }

    return 0;
}

char* concat(char* ubicacion, char* nombre){
    int size = strlen(ubicacion) + strlen(nombre) + 1;
    char* buf = malloc(size*sizeof(char));
    memset(buf, '\0', size);
    int i=0;
    for(; i<strlen(ubicacion); i++){
        buf[i] = *(ubicacion + i);
    }
    if(buf[i-1] != '/')
        buf[i] = '/';
    buf = strcat(buf, nombre);
    return buf;
}

int createDataBase(char* ubicacion, char* nombre){
    char* database = concat(ubicacion, nombre);
    int fd = open(database, O_RDONLY);
    int err;
    if(fd > 0) // en caso de que ya exista la base de datos
        goto errout;
    //crear archivo de base de datos    
    umask(0);
    fd = open(database, O_CREAT, 0666);
    if(fd < 0) // si hay un error
        goto errout;
    else
        close(fd);
    //crear indice
    char ind[1000];
    memset(ind, '\0', 1000);
    strcat(ind, "index_");
    strcat(ind, nombre);
    char* index = concat(ubicacion, ind);
    int ifd = open(index, O_CREAT, 0666);
    if(ifd < 0) // si hay un error
        goto errout;
    else
        close(ifd);


    //Guardar en configuracion 
    char* config = "configuracion";
    int tam = strlen(ubicacion) + strlen(config) + 1;
    char* ubicacionConfig = calloc(tam,sizeof(char));
    strcpy(ubicacionConfig,ubicacion);
    strcat(ubicacionConfig,config);

    char* indicestxt = "index_";
    char* comatxt = ",";
    int size = strlen(nombre) + strlen(comatxt) + strlen(indicestxt) + strlen(nombre) + strlen(comatxt) + strlen(ubicacion) + 1;
    char* todo = calloc(size,sizeof(char));
    strcpy(todo,nombre);
    strcat(todo,comatxt);
    strcat(todo,indicestxt);
    strcat(todo,nombre);
    strcat(todo,comatxt);
    strcat(todo,ubicacion);
    fd = open(ubicacionConfig, O_WRONLY|O_APPEND|O_CREAT, 0666);
    ssize_t a = write(fd, todo, strlen(todo)*sizeof(char));
    write(fd, "\n", strlen("\n")*sizeof(char));
    if(a < 0)
        goto errout;
    close(fd);
    free(todo);

    return 0;

    errout:
        err = errno;
        close(fd);
        errno = err;
        return (-1);
}

int openDataBase(char* ubicacion, char* nombre){
    int size = strlen(ubicacion) + strlen(nombre) + 1;
    char* database = calloc(size,sizeof(char));
    strcpy(database,ubicacion);
    strcat(database,nombre);

    char* indicestxt = "index_";
    size = strlen(ubicacion) + strlen(nombre) + strlen(indicestxt) + 1;
    char* indicesDB = calloc(size,sizeof(char));
    strcpy(indicesDB,ubicacion);
    strcat(indicesDB,indicestxt);
    strcat(indicesDB,nombre);
    
    umask(0);
    int fd = open(database, O_RDONLY);
    int err;
    if(fd < 0) // en caso que no exista la base de datos 
        goto errout;
    free(database);

    fd = open(indicesDB, O_RDONLY);
    if(fd < 0) // en caso que no exista indices de la base de datos 
        goto errout;
    free(indicesDB);

    //cargar indices
    char ind[1000];
    char key[500] = {0};
    char value[500] = {0};
    int variable = 0;
    memset(ind, '\0', 1000);
    size_t n = read(fd, ind, 1000);
    
    while(n != 0){
        if(n<0)
            goto errout;
        for(int i =0; i<n; i++){
            while(ind[i] != ':'){
                key[variable++] = ind[i];
                i++;
            }
            i++;
            variable = 0;
            while(ind[i] != '\n'){
                value[variable++] = ind[i];
                i++;
            }
            indices_struct *dato = malloc(sizeof(indices_struct));
            strcpy(dato->clave, key);
            dato->valor = atoi(value);
            HASH_ADD_STR(tabla, clave, dato);

            memset(key, '\0', 500);
            memset(value, '\0', 500);
            variable = 0;
        }
        memset(ind, '\0', 1000);
        n = read(fd, ind, 1000);
    }
    close(fd);
    return n;

    errout:
        err = errno;
        close(fd);
        errno = err;
        return (-1);
}

int putValue(char* ubicacion, char* nombre, char* key, char* value){
    int size = strlen(ubicacion) + strlen(nombre) + 1;
    char* database = calloc(size, sizeof(char));
    strcpy(database, ubicacion);
    strcat(database, nombre);  
    int fd = open(database, O_WRONLY|O_APPEND);

    int size2 = size + strlen("index_");
    char* index = calloc(size2, sizeof(char));
    strcpy(index, ubicacion);
    strcat(index, "index_");
    strcat(index, nombre);
    if(fd < 0)
        return -1;
                //clave + : + valor + \0
    int size1 = strlen(key) + 1 + strlen(value) + 1;
    char* string = calloc(size1, sizeof(char));

    __off_t cursor = lseek(fd, 0, SEEK_END); //posicion actual del cursor
    strcpy(string, key);
    strcat(string, ":");
    strcat(string, value);
    ssize_t a = write(fd, string, strlen(string)*sizeof(char));
    write(fd, "\n", strlen("\n")*sizeof(char));
    if(a < 0)
        return -1;
    close(fd);
    indices_struct* dato = malloc(sizeof(indices_struct));
    strcpy(dato->clave, key);
    dato->valor = cursor;
    
    indices_struct* busqueda  = NULL;
    indices_struct* viejo  = NULL;
    HASH_FIND_STR(tabla, key, busqueda);
    if(busqueda != NULL){
        HASH_REPLACE_STR(tabla, clave, dato, viejo);
        indices_struct *elemento, *tmp;
        int fdi = open(index, O_WRONLY|O_TRUNC|O_APPEND);
        //reescribir archivo de indices
        HASH_ITER(hh, tabla, elemento, tmp){
            int size3 = strlen(elemento->clave) + 2;
            char* string2 = calloc(size3, sizeof(char));
            strcpy(string2, elemento->clave);
            strcat(string2, ":");
            write(fdi, string2, strlen(string2)*sizeof(char));
            char tmp[100] = {'\0'};
            sprintf(tmp, "%d", elemento->valor);
            write(fdi, tmp, strlen(tmp)*sizeof(char));
            write(fdi, "\n", strlen("\n")*sizeof(char));
        }
        close(fdi);
        free(viejo);
    }else{
        HASH_ADD_STR(tabla, clave, dato);
        //agregar al final del archivo de indicies
        int fdi = open(index, O_WRONLY|O_APPEND);
        int size3 = strlen(key) + 2;
        char* string2 = calloc(size3, sizeof(char));
        strcpy(string2, key);
        strcat(string2, ":");
        write(fdi, string2, strlen(string2)*sizeof(char));
        char tmp[100] = {'\0'};
        sprintf(tmp, "%ld", cursor);
        write(fdi, tmp, strlen(tmp)*sizeof(char));
        write(fdi, "\n", strlen("\n")*sizeof(char));
        close(fdi);
    }
    return 0;
}

char* getValue(char*ubicacion, char* nombre, char* key){
    int size = strlen(ubicacion) + strlen(nombre) + 1;
    char* database = calloc(size,sizeof(char));
    strcpy(database,ubicacion);
    strcat(database,nombre);
    
    umask(0);
    int fd = open(database, O_RDONLY);
    int err;
    if(fd < 0) // en caso que no exista la base de datos 
        goto errout;
    free(database);

    indices_struct *encontrado = NULL;
	HASH_FIND_STR(tabla, key, encontrado);
    if(encontrado == NULL)
        return NULL;

    //Leer de la Base de Datos
    char ind[1000];
    char clave[500] = {0};
    char* value = calloc(500,sizeof(char));;
    int variable = 0;
    lseek(fd, encontrado->valor, SEEK_SET); //Reubicar cursor

    size_t n = read(fd, ind, 1000);
    if(n<0)
        goto errout;
    int i = 0;
    while(ind[i] != ':'){
        clave[variable++] = ind[i];
        i++;
    }
    i++;
    variable = 0;
    while(ind[i] != '\n'){
        value[variable++] = ind[i];
        i++;
    }
    if(strcmp(clave,key)!=0 || strcmp("@T!",value) == 0)        
        return NULL;
    memset(clave, '\0', 500);
    close(fd);
    return value;

    errout:
        err = errno;
        close(fd);
        errno = err;
        return 0;
}

int delete(char* ubicacion, char* nombre, char* key){
    indices_struct* busqueda  = NULL;
    HASH_FIND_STR(tabla, key, busqueda);
    if(busqueda == NULL) // si no se encuentra la clave en el indice
        return -1;

    int size = strlen(ubicacion) + strlen(nombre) + 1;
    char* file = calloc(size, sizeof(char));
    strcpy(file, ubicacion);
    strcat(file, nombre);  
    int fd = open(file, O_RDWR|O_APPEND);
    if(fd < 0)
        return -1;
    
    lseek(fd, busqueda->valor, SEEK_SET);
    char buf[1000] = {'\0'};
    char value[500] = {'\0'};
    int byt = read(fd, buf, 1000);
    if(byt < 0)
        return -1;
    int i = 0;
    int c = -1;
    while(buf[i] != '\n'){
        if(buf[i] == ':')
            c++;
        if(c > -1 && buf[i+1] != '\n'){
            value[c++] = buf[i + 1];
        }
        i++;
    }
    //En caso de que ya se haya eliminado el elemento
    if(strcmp(value, "@T!") == 0){
        close(fd);
        return -1;
    }

    __off_t cursor = lseek(fd, 0, SEEK_END); //posicion actual del cursor

    write(fd, busqueda->clave, strlen(busqueda->clave)*sizeof(char));
    write(fd, ":@T!", 4 * sizeof(char));
    write(fd, "\n", strlen("\n")*sizeof(char));

    int size2 = size + strlen("index_");
    char* index = calloc(size2, sizeof(char));
    strcpy(index, ubicacion);
    strcat(index, "index_");
    strcat(index, nombre);

    indices_struct* dato = malloc(sizeof(indices_struct));
    strcpy(dato->clave, key);
    dato->valor = cursor;
    
    indices_struct* viejo  = NULL;

    HASH_REPLACE_STR(tabla, clave, dato, viejo);
    indices_struct *elemento, *tmp;
    int fdi = open(index, O_WRONLY|O_TRUNC|O_APPEND);
    if(fdi < 0){
        close(fd);
        return -1;
    }
    //reescribir archivo de indices
    HASH_ITER(hh, tabla, elemento, tmp){
        int size3 = strlen(elemento->clave) + 2;
        char* string2 = calloc(size3, sizeof(char));
        strcpy(string2, elemento->clave);
        strcat(string2, ":");
        write(fdi, string2, strlen(string2)*sizeof(char));
        char tmp[100] = {'\0'};
        sprintf(tmp, "%d", elemento->valor);
        write(fdi, tmp, strlen(tmp)*sizeof(char));
        write(fdi, "\n", strlen("\n")*sizeof(char));
    }
    close(fdi);
    free(viejo);

    close(fd);
    return 0;
}