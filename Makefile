all: test_hashtable logdb cliente

test_hashtable: src/test_hashtable.c
	gcc  -Iinclude/ src/test_hashtable.c -o bin/test_hashtable


logdb: obj/servidor.o
	gcc -o bin/$@ $<

obj/servidor.o: src/servidor.c
	gcc -Wall -g -c $< -o $@ -Iinclude/


cliente: obj/cliente.o lib/liblogdb.so
	gcc $^ -llogdb -L lib/ -o bin/$@
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./lib/

lib/liblogdb.so: obj/logdb.o
	gcc -shared $^ -o $@

obj/logdb.o: src/logdb.c
	gcc -c -fPIC $^ -o $@

obj/cliente.o: src/cliente.c
	gcc -Wall -g -c $< -o $@ -I include/

.PHONY: clean
clean:
	rm bin/* obj/*
